'''
Created on Aug 25, 2013

@author: mnshkmr
Understanding Metaclass in Python
=================================
Can also create classes dynamically as one can do with objects.
Type is Python's metaclass and can be used to create classes on the fly. 
Signature of type:
       type(name of the class, 
       tuple of the parent class (for inheritance, can be empty), 
       dictionary containing attributes names and values)
What can be a metaclass?
'type' or anything that subclasses 'type'. 
'''

# A method that would be used as a metaclass for our custom class
def unicode_type(class_name, parent_class_name, attributes):
    new_attr = {}
    for name, val in attributes.items():
        if not name.startswith('__'):
            new_attr[name] = val.encode('utf8')
        else:
            new_attr[name] = val
    return type(class_name, parent_class_name, new_attr)

'''
The metaclass does not affect the objects instantiated by the class.
The value of the class attribute (defined in the class), gets changed to unicode.
but the attributes for objects remain unchanged.
'''

class CustomClassOne(metaclass=unicode_type):
    full_name = ""
    age = ""
    str2 = '深入 Python'
    def __init__(self, name_str, age_str) :
        self.full_name = name_str
        self.age = age_str
        
#Sample Test Code
one = CustomClassOne('深入 Name', "26")
print (one.full_name) # prints 深入 Python
print (one.str2) # prints b'\xe6\xb7\xb1\xe5\x85\xa5 Python'

#### How metaclass type can be used to create another class.
#### FooChild is a class created by FooChild, making new_func as 
#### a method of child class
def new_func():
    print ("This is a method in class Foo")

Foo = type('Foo', (), {})
FooChild = type('FooChild', (Foo,), {'fooChildFunc': new_func} )
print(FooChild.fooChildFunc()) #prints This is a method in class Foo
